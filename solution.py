import sys
import numpy as np
from collections import defaultdict, deque


# Dijkstra's Algorithm implementation borrowed from https://gist.github.com/mdsrosa/c71339cb23bc51e711d8
# Small fix applied to make no difference between A - B path and B - A path
# Extended to calculate cartesian product
class Graph(object):
    def __init__(self, number_of_nodes):
        self.node_names = range(number_of_nodes)
        self.nodes = set()
        self.edges = defaultdict(list)
        self.distances = {}

    def add_node(self, value):
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, distance):
        self.edges[from_node].append(to_node)
        self.edges[to_node].append(from_node)
        self.distances[(from_node, to_node)] = distance
        self.distances[(to_node, from_node)] = distance

    def get_nodes(self):
        return self.node_names

    def dijkstra(self, initial):
        visited = {initial: 0}
        path = {}

        nodes = set(self.nodes)

        while nodes:
            min_node = None
            for node in nodes:
                if node in visited:
                    if min_node is None:
                        min_node = node
                    elif visited[node] < visited[min_node]:
                        min_node = node
            if min_node is None:
                break

            nodes.remove(min_node)
            current_weight = visited[min_node]

            for edge in self.edges[min_node]:
                try:
                    weight = current_weight + self.distances[(min_node, edge)]
                except:
                    continue
                if edge not in visited or weight < visited[edge]:
                    visited[edge] = weight
                    path[edge] = min_node

        return visited, path

    def shortest_path(self, origin, destination):
        visited, paths = self.dijkstra(origin)
        full_path = deque()
        _destination = paths[destination]

        while _destination != origin:
            full_path.appendleft(_destination)
            _destination = paths[_destination]

        full_path.appendleft(origin)
        full_path.append(destination)

        return visited[destination], list(full_path)

    def calculate_min_paths_for_nodes_cartesian_product(self):
        result_min_paths = {}

        for i in self.get_nodes():
            for j in self.get_nodes():
                # The same reason as before we don't need to calculate the same twice for symmetric matrix
                if i > j:
                    calculated_shortest_path_with_weight = self.shortest_path(i, j)

                    calculated_min_path_weight = calculated_shortest_path_with_weight[0]
                    reversed_calculated_min_path = list(reversed(calculated_shortest_path_with_weight[1]))

                    result_min_paths[(i, j)] = calculated_shortest_path_with_weight
                    result_min_paths[(j, i)] = (calculated_min_path_weight, reversed_calculated_min_path)

        return result_min_paths


class MinGraphTraversalPathCalculator(object):
    def __init__(self, node_names_param, min_paths_between_all_nodes_param):
        self.all_nodes_set = set(node_names_param)
        self.min_paths_between_all_nodes = min_paths_between_all_nodes_param

        # Could be None but decided to use a large number to simplify code a bit
        self.min_path = (999999999, [])

    def calculate_min_path_through_all_nodes(self, current_node, visited_nodes, path_weight):
        not_visited_nodes = self.all_nodes_set - set(visited_nodes)

        min_path_weight = self.min_path[0]

        if len(not_visited_nodes) == 0:
            result_weight = path_weight

            if current_node != 0:
                result_weight = result_weight + self.min_paths_between_all_nodes[(current_node, 0)][0]
                visited_nodes.append(0)

            if result_weight < min_path_weight:
                return result_weight, visited_nodes
            else:
                return None
        else:
            for not_visited_node in not_visited_nodes:
                path_from_current_to_not_visited = self.min_paths_between_all_nodes[(current_node, not_visited_node)]

                new_weight = path_weight + path_from_current_to_not_visited[0]

                if new_weight > min_path_weight:
                    continue

                new_visited_nodes = visited_nodes + path_from_current_to_not_visited[1][1:]

                intermediate_result = self.calculate_min_path_through_all_nodes(not_visited_node, new_visited_nodes, new_weight)

                if intermediate_result is not None and intermediate_result[0] < min_path_weight:
                    self.min_path = intermediate_result

        return self.min_path


def read_n_and_matrix_from_input():
    result_n = None
    result_matrix = []

    index = 0
    for line in sys.stdin:
        if index == 0:
            result_n = int(line)
        else:
            weights_as_str = line.split()
            result_matrix.append(list(map(lambda weight: int(weight), weights_as_str)))

        index = index + 1

    return result_n, result_matrix


def create_graph(number_of_nodes, weights_matrix):
    result_graph = Graph(number_of_nodes)

    nodes = result_graph.get_nodes()
    for i in nodes:
        result_graph.add_node(i)

        for j in nodes:
            # We need elements only above the main diagonal because our matrix is symmetric
            if i >= j:
                weight = weights_matrix[i][j]

                has_edge_between_nodes = weight != 0

                if has_edge_between_nodes:
                    result_graph.add_edge(i, j, weight)

    return result_graph


(n, distance_matrix) = read_n_and_matrix_from_input()

graph = create_graph(n, distance_matrix)

min_paths_between_all_nodes = graph.calculate_min_paths_for_nodes_cartesian_product()

min_path_calculator = MinGraphTraversalPathCalculator(graph.get_nodes(), min_paths_between_all_nodes)

result_min_path = min_path_calculator.calculate_min_path_through_all_nodes(0, [0], 0)

min_weight = result_min_path[0]

print(min_weight)
