# USML Task 1.4

As a basement standard Dijkstra's Algorithm is used to calculate min path from one node to another.

The main idea behind the solution is that we starting from a node and know nodes where we haven't go yet.
As soon as we pre-calculated min paths between all nodes we can go recursively to each not visited node by its min path.
At the same time we're updating visited nodes with ones that are on the min path to the node and also updating weight.
When we went through all nodes we need to go back to the first one.
When we went through all combinations that deserve attention we have the min path.

There's an optimization that we don't calculate the rest of the path if it's already longer than the last min path.